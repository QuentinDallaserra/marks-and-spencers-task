import {
  GET_CART_ITEM_COUNT,
  UPDATE_CART_ITEM,
  DELIVERY_CHARGES,
  ANIMATE_PRODUCT
} from "../constants/shop";
import {
  removeCartItem,
  applyCartItemUpdate,
  updateCartTotalPrice,
  removeProductAnimation
} from "../actions/shop";

export default store => next => action => {
  switch (action.type) {
    case GET_CART_ITEM_COUNT:
      const currentCartCode = action.payload;
      if (store.getState().shop.itemsInCart[currentCartCode].count <= 0) {
        store.dispatch(removeCartItem(currentCartCode));
      }
      return next(action);
    case UPDATE_CART_ITEM:
      store.dispatch(applyCartItemUpdate(action.payload));
      const itemTotal = Object.values(store.getState().shop.itemsInCart)
        .map(entry => entry.count * entry.data.price)
        .reduce((accumulator, currentValue) => accumulator + currentValue);
      store.dispatch(updateCartTotalPrice(itemTotal + DELIVERY_CHARGES));
      return next(action);
    case ANIMATE_PRODUCT:
      setTimeout(() => {
        store.dispatch(removeProductAnimation());
      }, 600);
      return next(action);
    default:
      return next(action);
  }
};
