import React, { Component } from "react";
import { connect } from "react-redux";

import Title from "../components/Title";
import ProductListing from "../components/ProductListing";

class Shop extends Component {
  render() {
    return (
      <div className="main-content">
        <Title title="Shop" />
        <ProductListing />
      </div>
    );
  }
}

export default connect()(Shop);
