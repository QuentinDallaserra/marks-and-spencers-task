import React, { Component } from "react";
import { connect } from "react-redux";

import "../scss/index.scss";

import Header from "./components/Header";
import Shop from "./views/Shop";

class App extends Component {
  render() {
    return (
      <>
        <Header />
        <Shop />
      </>
    );
  }
}

export default connect()(App);
