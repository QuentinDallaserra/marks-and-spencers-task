import React from "react";
import Basket from "./Basket";
import Cart from "./Cart";
import ProductAnimation from "./ProductAnimation";

const Header = () => (
  <header className="header">
    <h3 className="header__title">Simple Fashion</h3>
    <Cart />
    <Basket />
    <ProductAnimation />
  </header>
);

export default Header;
