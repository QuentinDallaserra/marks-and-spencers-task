import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { toggleCartVisibility } from "../actions/shop";

const mapStateToProps = state => {
  return {
    cartItemCount: state.shop.cartItemCount
  };
};

const mapDispatchToProps = dispatch => ({
  toggleCartVisibility: () => dispatch(toggleCartVisibility())
});

class Basket extends Component {
  render() {
    const { toggleCartVisibility, cartItemCount } = this.props;
    const basketEmpty = cartItemCount !== 0 ? 'basket--has-items': 'basket--is-empty'

    return (
      <button
        className={`basket ${basketEmpty}`}
        data-attribute={cartItemCount}
        onClick={() => {
          toggleCartVisibility(false);
        }}
      />
    );
  }
}

Basket.propTypes = {
  cartItemCount: PropTypes.number.isRequired,
  toggleCartVisibility: PropTypes.func,
};

Basket.defaultProps = {
  toggleCartVisibility: () => {},
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Basket);
