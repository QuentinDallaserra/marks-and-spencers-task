import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import { updateCartItem, updateCartItemsCount, animateProduct } from "../actions/shop";
import { formatDecimalPrice } from "../api/shop";
import { STORE_CURRENCY } from "../constants/shop";

const mapStateToProps = state => {
  return {
    itemsInCart: state.shop.itemsInCart,
    cartItemCount: state.shop.cartItemCount
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ updateCartItemsCount, updateCartItem, animateProduct }, dispatch) };
};

class ProductTease extends Component {
  handleUpdateCartItem = item => {
    const { itemsInCart, cartItemCount } = this.props;
    const { updateCartItem, updateCartItemsCount } = this.props.actions;

    const filterItemsInCart = Object.keys(itemsInCart).filter(key => item.code.includes(key));
    const currentItemInCart = itemsInCart[filterItemsInCart];
    const currentItemCount = (currentItemInCart && currentItemInCart.count) || 0;

    updateCartItem(item, currentItemCount + 1);
    updateCartItemsCount(cartItemCount + 1);
  };

  handleProductAnimation = item => {
    const { animateProduct } = this.props.actions;
    animateProduct(item.image);
  };

  render() {
    const { item } = this.props;

    return (
      <div className="product-tease">
        <img src={item.image} className="product-tease__image" alt={item.name} />

        <div className="product-tease__container">
          <div className="product-tease__info">
            <span className="product-tease__name">{item.name}</span>
            <span className="product-tease__price">{STORE_CURRENCY + formatDecimalPrice(item.price)}</span>
          </div>

          <button
            className="product-tease__basket"
            onClick={() => {
              this.handleUpdateCartItem(item);
              this.handleProductAnimation(item);
            }}
          />
        </div>
      </div>
    );
  }
}

ProductTease.propTypes = {
  item: PropTypes.object,
  itemsInCart: PropTypes.object,
  cartItemCount: PropTypes.number.isRequired,
  actions: PropTypes.object.isRequired
};

ProductTease.defaultProps = {
  items: {},
  itemsInCart: {}
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductTease);
