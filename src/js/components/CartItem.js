import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { updateCartItemsCount, updateCartItem, getCartItemCount } from "../actions/shop";
import { formatDecimalPrice } from "../api/shop";
import { STORE_CURRENCY, INCREMENT, DECREMENT } from "../constants/shop";

const mapStateToProps = state => {
  return {
    isCartVisible: state.shop.isCartVisible,
    itemsInCart: state.shop.itemsInCart,
    cartItemCount: state.shop.cartItemCount
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ updateCartItemsCount, updateCartItem, getCartItemCount }, dispatch) };
};

class CartItem extends Component {
  handleIncrement = (item, incrementType) => {
    const { itemsInCart, cartItemCount } = this.props;
    const { updateCartItem, updateCartItemsCount, getCartItemCount } = this.props.actions;

    const filterItemsInCart = Object.keys(itemsInCart).filter(key => item[0].includes(key));
    const currentItemInCart = itemsInCart[filterItemsInCart];
    const currentItemCount = currentItemInCart && currentItemInCart.count;

    updateCartItem(item[1].data, incrementType === INCREMENT ? currentItemCount + 1 : currentItemCount - 1);
    updateCartItemsCount(incrementType === INCREMENT ? cartItemCount + 1 : cartItemCount - 1);
    getCartItemCount(item[0]);
  };

  render() {
    const { item } = this.props;

    return (
      <div className="cart-item">
        <div className="cart-item__image" style={{ backgroundImage: `url(${item[1].data.image})` }} />
        <div className="cart-item__info">
          <span className="cart-item__name">{item[1].data.name}</span>
          <span className="cart-item__price">{STORE_CURRENCY + formatDecimalPrice(item[1].data.price)}</span>
        </div>
        <div className="cart-item__counter">
          <button onClick={() => this.handleIncrement(item, DECREMENT)}>-</button>
          <span>{item[1].count}</span>
          <button onClick={() => this.handleIncrement(item, INCREMENT)}>+</button>
        </div>
      </div>
    );
  }
}

CartItem.propTypes = {
  item: PropTypes.array.isRequired,
  actions: PropTypes.object.isRequired,
  itemsInCart: PropTypes.object.isRequired,
  cartItemCount: PropTypes.number.isRequired,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CartItem);
