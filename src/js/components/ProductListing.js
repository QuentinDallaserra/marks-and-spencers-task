import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { getData } from "../actions/shop";

import ProductTease from "./ProductTease";

const mapStateToProps = state => {
  return {
    shopItems: state.shop.shopItems
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ getData }, dispatch) };
};

class ProductListing extends Component {

  componentDidMount() {
    this.props.actions.getData();
  }

  render() {
    const { shopItems } = this.props;

    return <div className="product-listing">{shopItems && shopItems.map(item => <ProductTease item={item} key={item.code}/>)}</div>;
  }
}

ProductListing.propTypes = {
  shopItems: PropTypes.array,
  actions: PropTypes.object.isRequired,
};

ProductListing.defaultProps = {
  shopItems: [],
};


export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProductListing);
