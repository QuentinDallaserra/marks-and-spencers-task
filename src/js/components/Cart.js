import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { STORE_CURRENCY, DELIVERY_CHARGES } from "../constants/shop";
import { toggleCartVisibility } from "../actions/shop";
import { formatDecimalPrice } from "../api/shop";
import CartItem from "./CartItem";
import Button from "./Button";

const mapStateToProps = state => {
  return {
    isCartVisible: state.shop.isCartVisible,
    itemsInCart: state.shop.itemsInCart,
    cartTotalPrice: state.shop.cartTotalPrice
  };
};

const mapDispatchToProps = dispatch => {
  return { actions: bindActionCreators({ toggleCartVisibility }, dispatch) };
};

class Cart extends Component {
  render() {
    const { itemsInCart, cartTotalPrice, isCartVisible } = this.props;

    const cartVisiblityClassName = isCartVisible ? "cart__container--is-visible" : "";

    return (
      <div className="cart">
        {isCartVisible && <div className="cart__opacity" onClick={this.props.actions.toggleCartVisibility} />}

        <div className={`cart__container ${cartVisiblityClassName}`}>
          <h3 className="cart__title">My basket</h3>
          {Object.keys(itemsInCart).length !== 0 ? (
            <>
              {Object.entries(itemsInCart).map(item => (
                <CartItem item={item} key={item} />
              ))}
              <div className="cart__delivery">
                Delivery charges:<span>{STORE_CURRENCY + DELIVERY_CHARGES}</span>
              </div>
              <div className="cart__results">
                <div className="cart__total">Total</div>
                <div className="cart__sum">{STORE_CURRENCY + formatDecimalPrice(cartTotalPrice)}</div>
              </div>
              <Button type="filled" name="Checkout" className="cart__button" />
            </>
          ) : (
            <div className="cart__empty">Your basket is empty</div>
          )}
        </div>
      </div>
    );
  }
}

Cart.propTypes = {
  itemsInCart: PropTypes.object,
  isCartVisible: PropTypes.bool.isRequired,
  cartTotalPrice: PropTypes.number.isRequired
};

Cart.defaultProps = {
  itemsInCart: {}
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Cart);
