import React, { Component } from "react";
import { connect } from "react-redux";
import PropTypes from "prop-types";

const mapStateToProps = state => {
  return {
    productAnimation: state.shop.productAnimation
  };
};

class ProductAnimation extends Component {
  render() {
    const { productAnimation } = this.props;
    return productAnimation.productIsAnimating && <div className="product-animation" style={{backgroundImage: `url(${productAnimation.imageUrl})`}}/>;
  }
}

ProductAnimation.propTypes = {
  productAnimation: PropTypes.object,
};

ProductAnimation.defaultProps = {
  productAnimation: {},
};

export default connect(
  mapStateToProps,
  null
)(ProductAnimation);
