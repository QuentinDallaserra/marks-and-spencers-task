import React from "react";
import PropTypes from "prop-types";

const Button = ({ name, type, className }) => {
  const buttonType = `button--${type}`;

  return <button className={`button ${buttonType} ${className}`}>{name}</button>;
};

Button.propTypes = {
  name: PropTypes.string.isRequired,
  type: PropTypes.string.isRequired,
  className: PropTypes.string,
};

Button.defaultProps = {
  className: '',
};

export default Button;
