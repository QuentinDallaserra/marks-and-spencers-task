import { 
  GET_DATA,
  UPDATE_CART_ITEM,
  TOGGLE_CART_VISIBILITY,
  UPDATE_CART_ITEMS_COUNT,
  APPLY_CART_ITEMS_UPDATE,
  UPDATE_CART_TOTAL_PRICE,
  GET_CART_ITEM_COUNT,
  REMOVE_CART_ITEM,
  SHOP_ITEMS,
  ANIMATE_PRODUCT,
  REMOVE_PRODUCT_ANIMATION
} from "../constants/shop";

export const getData = () => dispatch => {
  dispatch({
    type: GET_DATA,
    payload: SHOP_ITEMS
  });
};

export const updateCartItem = (item, count) => dispatch => {
  dispatch({
    type: UPDATE_CART_ITEM,
    payload: {
      data: item,
      count
    }
  });
};

export const animateProduct = imageUrl => dispatch => {
  dispatch({
    type: ANIMATE_PRODUCT,
    payload: {
      imageUrl,
      productIsAnimating: true
    }
  });
};

export const removeProductAnimation = () => dispatch => {
  dispatch({
    type: REMOVE_PRODUCT_ANIMATION,
    payload: {
      imageUrl: null,
      productIsAnimating: false
    }
  });
};

export const applyCartItemUpdate = payload => dispatch => {
  dispatch({
    type: APPLY_CART_ITEMS_UPDATE,
    payload
  });
};

export const updateCartTotalPrice = total => dispatch => {
  dispatch({
    type: UPDATE_CART_TOTAL_PRICE,
    payload: total
  });
};

export const updateCartItemsCount = count => dispatch => {
  dispatch({
    type: UPDATE_CART_ITEMS_COUNT,
    payload: count
  });
};

export const getCartItemCount = code => dispatch => {
  dispatch({
    type: GET_CART_ITEM_COUNT,
    payload: code
  });
};

export const removeCartItem = code => dispatch => {
  dispatch({
    type: REMOVE_CART_ITEM,
    payload: code
  });
};

export const toggleCartVisibility = () => dispatch => {
  dispatch({
    type: TOGGLE_CART_VISIBILITY
  });
};
