import { 
  GET_DATA,
  APPLY_CART_ITEMS_UPDATE,
  TOGGLE_CART_VISIBILITY,
  UPDATE_CART_ITEMS_COUNT,
  UPDATE_CART_TOTAL_PRICE,
  REMOVE_CART_ITEM,
  ANIMATE_PRODUCT,
  REMOVE_PRODUCT_ANIMATION,
} from "../constants/shop";

const initialState = {
  cartItemCount: 0,
  cartTotalPrice: 0,
  itemsInCart: {},
  isCartVisible: false,
  productAnimation: {
    productIsAnimating: false
  }
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_DATA:
      return {
        ...state,
        shopItems: action.payload
      };
    case APPLY_CART_ITEMS_UPDATE:
      return {
        ...state,
        itemsInCart: {
          ...state.itemsInCart,
          [action.payload.data.code]: action.payload
        }
      };
    case UPDATE_CART_TOTAL_PRICE:
      return {
        ...state,
        cartTotalPrice: action.payload
      };
    case ANIMATE_PRODUCT:
      return {
        ...state,
        productAnimation: action.payload
      };
    case REMOVE_PRODUCT_ANIMATION:
      return {
        ...state,
        productAnimation: action.payload
      };
    case UPDATE_CART_ITEMS_COUNT:
      return {
        ...state,
        cartItemCount: action.payload
      };
    case TOGGLE_CART_VISIBILITY:
      return {
        ...state,
        isCartVisible: !state.isCartVisible
      };
    case REMOVE_CART_ITEM:
      const newState = {
        ...state
      };
      delete newState.itemsInCart[action.payload];
      return {
        ...newState
      };
    default:
      return state;
  }
};
