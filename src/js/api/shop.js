export const formatDecimalPrice = price => parseFloat(Math.round(price * 100) / 100).toFixed(2);
