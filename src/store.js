import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducers from "./js/reducers";
import middleware from './js/middleware';

const enhancedComposer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default function configureStore(initialState = {}) {
  return createStore(
    reducers,
    initialState,
    enhancedComposer(applyMiddleware(thunk, middleware.shop)));
}
