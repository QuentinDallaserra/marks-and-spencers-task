## Marks and Spencers REACT task

A REACT task by Quentin Dallaserra for Marks and Spencers

To install the npm packages: `$npm install`
Then to run the project: `$npm start`

Notes:

- I was not given the font used in the design and could not find the correct one so I used Arial

- The product names fit on multiple lines which doesn't match the design, this is something I would need to talk to the UI designer on how he/she would want to approach this. Or give my own input if wanted.

- The product names had a code adjacent right in the designs, I could not find this code in the JSON object.

- Ran out of time and so didn't animate the red counter :(

- Given more time I would have used more CSS utility classes IE for font-sizes, text-colors, etc...

For any questions you can contact me at [hello@quentin.xyz](hello@quentin.xyz)
